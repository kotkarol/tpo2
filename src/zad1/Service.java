/**
 *
 *  @author Kot Karol S15259
 *
 */

package zad1;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.jsoup.Jsoup;

public class Service {
    private String country;

    public Service(String country) throws Exception {

        if (country.length()  != 3)
        {
            throw new Exception("There must be ISO 3166 format county code.");
        }
        this.country = country;
    }

    public String getWeather(String cityName) throws Exception {

        try {
            String apiKey = "9c610676ba86a9718732a2e059e56c58";
            String url = "https://api.openweathermap.org/data/2.5/weather?q=" + country + "," +  cityName + "&APIKEY=" + apiKey;

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("GET");

            BufferedReader in = null;
            try {
                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            String inputLine = "";
            StringBuilder response = new StringBuilder();

            while (true)
            {
                if ((inputLine = in.readLine()) == null) break;
                response.append(inputLine);
            }
            response.append(inputLine);
            in.close();
            return response.toString();
        }
        catch (Exception ex)
        {
            throw new Exception("Service.getWeather - has thrown exception: " + ex.getMessage());
        }
    }

    public Double getRateFor(String currency) throws Exception {
        try {
            //URL url = new URL("http://www.nbp.pl/kursy/kursya.html");
            if (currency.length() != 3) return -1.0;
            CurrencyFinder cf = new CurrencyFinder(country);
            String currencyCode = cf.Get_Currency_Code();
            if(currencyCode == "" || currencyCode == null)
            {
                throw new Exception("Currency hasn't been found!");
            }
            URL url = new URL("https://api.exchangeratesapi.io/latest?symbols="+ currencyCode + "," + currency);

            URLConnection con = url.openConnection();
            InputStream in = con.getInputStream();
            String encoding = con.getContentEncoding();  // ** WRONG: should use "con.getContentType()" instead but it returns something like "text/html; charset=UTF-8" so this value must be parsed to extract the actual encoding
            encoding = encoding == null ? "UTF-8" : encoding;

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buf = new byte[8192];
            int len = 0;
            while ((len = in.read(buf)) != -1) {
                baos.write(buf, 0, len);
            }
            String body = new String(baos.toByteArray(), encoding);

            return 0.0;
        }
        catch (Exception ex)
        {
            throw new Exception("Service.getRateFor - has throw exception: " + ex.getMessage());
        }
    }

    public Double getNBPRate() throws IOException {
        //URL url = new URL("http://www.nbp.pl/kursy/kursya.html");

        CurrencyFinder currencyFinder = new CurrencyFinder("GBR");
        String currencyCode = currencyFinder.Get_Currency_Code();

        if (country.toUpperCase().equals("POL")) return 1.0;
        ArrayList<String> pageList = new ArrayList<>();
        pageList.add("http://www.nbp.pl/kursy/kursya.html");
        pageList.add("http://www.nbp.pl/kursy/kursyb.html");

        boolean activated = false;

        for (String url: pageList)
        {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).get();
            org.jsoup.select.Elements rows = doc.select("tr");
            for(org.jsoup.nodes.Element row :rows)
            {
                org.jsoup.select.Elements columns = row.select("td");
                for (org.jsoup.nodes.Element column:columns)
                {
                    if (!column.hasText())continue;
                    if (column.text().toUpperCase().contains(currencyCode))
                    {
                        int index = column.text().indexOf(currencyCode);
                        String tekst = column.text().substring(index + 3);
                        String[] splitted = tekst.split("([a-z]|[A-Z])");

                        Double dobule = new Double(splitted[0].replace(" ","").replace(",","."));
                        return dobule;
                    }
                }
            }
        }
        return 0.0;
    }
}
