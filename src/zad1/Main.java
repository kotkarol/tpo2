/**
 *
 *  @author Kot Karol S15259
 *
 */

package zad1;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class Main extends Application{
  public static void main(String[] args) throws Exception {
    Service s = new Service("POL");
    String weatherJson = s.getWeather("Warsaw");
    Double rate1 = s.getRateFor("USD");
    Double rate2 = s.getNBPRate();
    // ...
    // część uruchamiająca GUI
    launch();
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    Label label = new Label("Hello World");
    label.setAlignment(Pos.CENTER);
    Scene scene = new Scene(label, 500, 350);


    primaryStage.setTitle("TPO 2");
    primaryStage.setScene(scene);
    primaryStage.show();
  }
}
