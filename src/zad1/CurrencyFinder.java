package zad1;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

public class CurrencyFinder {


    private String countryName;

    public CurrencyFinder(String countryName)
    {
        if(countryName.length() != 3) return;
        this.countryName = countryName;
    }
    public String Get_Currency_Code() throws IOException {
        String pathString = "D:\\SzkolneProjekty\\TPO2_KK_S15259\\resources\\CountryAndCurrencies";
        Path path = Paths.get(pathString);
        String content = Read_All_File(path);
        String[] lines = content.split("\\r?\\n");

        for (String line: lines) {
            String[] slitted = line.split("[,]");
            if (slitted.length != 2) continue;

            if (countryName.toUpperCase().equals(slitted[0]))
            {
                return slitted[1];
            }
        }
        return "";
    }
    public String Read_All_File(Path path) throws IOException {
        String output;
        File file = new File(String.valueOf(path));
        FileInputStream fis = new FileInputStream(file);
        try {
            byte[] data = new byte[(int) file.length()];
            fis.read(data);
            output = new String(data, "UTF-8");
        }
        catch (IOException ex)
        {
            throw new IOException("Read_All_File has thrown exception: " + ex.getMessage());
        }
        finally {
            fis.close();
        }
       return output;
    }
}
