package zad1;

import org.junit.Assert;

import java.io.IOException;

import static org.junit.Assert.*;

public class ServiceTest {

    @org.junit.Test
    public void getWeather() throws Exception {
        Service service = new Service("POL");
        try {
            String infos = service.getWeather("Warsaw");
            Assert.assertNotNull(infos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @org.junit.Test
    public void getRateFor() throws Exception {
        Service service = new Service("GBR");
        try {
            Double infos = service.getRateFor("USD");
            Assert.assertNotNull(infos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @org.junit.Test
    public void getNBPRateForPoland() throws Exception {
        Service service = new Service("POL");
        try {
            Double infos = service.getNBPRate();
            Double liczba = 1.0;
            Assert.assertEquals(liczba, infos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @org.junit.Test
    public void getNBPRate() throws Exception {
        Service service = new Service("GBR");
        try {
            Double infos = service.getNBPRate();
            Assert.assertNotNull(infos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
